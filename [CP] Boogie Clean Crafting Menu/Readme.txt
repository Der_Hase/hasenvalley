Values for the ini file:

		Language Settings:"English, German, Spanish, Portuguese, Russian"

		Tidy Menu: "true, false"

		Fertilizer:"true, false"

		Wild Bait:"5x, 3x, 1x, OFF"

		Rare Seed:"true, false"

		Burglar Ring:"true, false"

		Bombs:"2 Ore, 3 Ore, 4 Ore, OFF"

		Ammo:"2 Ore, 3 Ore, 4 Ore, 5 Ore, OFF"

		Golden Mega Bomb:"true, false"


For more info and update: 

https://www.nexusmods.com/stardewvalley/mods/7177?tab=description